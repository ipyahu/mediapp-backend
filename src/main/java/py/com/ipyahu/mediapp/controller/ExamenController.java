package py.com.ipyahu.mediapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.com.ipyahu.mediapp.exception.ModeloNotFoundException;
import py.com.ipyahu.mediapp.model.Examen;
import py.com.ipyahu.mediapp.service.ExamenService;

@RestController
@RequestMapping("/examenes")
@Api(value="Servicio REST para examenes")
public class ExamenController {

	@Autowired
	private ExamenService service;
	
	@ApiOperation("Retorna una lista de examenes.")
	@GetMapping(produces= MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<List<Examen>> listar (){
		return new ResponseEntity<List<Examen>>(service.listar(), HttpStatus.OK);
	}
	
	@ApiOperation("Retorna un examen por su ID.")
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Examen> listarId(@PathVariable("id") Integer id){
		Examen obj = service.listarId(id);
		if (obj == null){
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<Examen>(service.listarId(id), HttpStatus.OK);
	}
	
	@ApiOperation("Permite el registro de un examen.")
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Examen> registrar(@Valid @RequestBody Examen obj){
		return new ResponseEntity<Examen>(service.registrar(obj), HttpStatus.CREATED);
	}
	
	@ApiOperation("Permite la modificacion de un examen.")
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Examen> modificar (@Valid @RequestBody Examen obj){
		return new ResponseEntity<Examen>(service.modificar(obj), HttpStatus.OK);
	}
	
	@ApiOperation("Permite la eliminación de un examen.")
	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable("id") Integer id){
		Examen obj = service.listarId(id);
		if (obj == null){
			throw new ModeloNotFoundException("ID: " + id);
		}
		else{
			service.eliminar(id);
		}
	}
}
