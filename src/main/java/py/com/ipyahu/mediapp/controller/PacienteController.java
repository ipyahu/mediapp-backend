package py.com.ipyahu.mediapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.com.ipyahu.mediapp.exception.ModeloNotFoundException;
import py.com.ipyahu.mediapp.model.Paciente;
import py.com.ipyahu.mediapp.service.PacienteService;

@RestController
@RequestMapping("/pacientes")
@Api(value="Servicio REST para pacientes")
public class PacienteController {

	/*
	 * EN ALGUNOS METODOS YA NO DEFINIMOS NINGUNA RUTA PARA LA PROPIEDAD
	 * VALUE PORQUE USAREMOS EN ESENCIA LOS METODOS HTTP
	 * */
	
	@Autowired
	private PacienteService service;
	
	@ApiOperation("Retorna una lista de pacientes.")
	@GetMapping(produces= MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<List<Paciente>> listar (){
		return new ResponseEntity<List<Paciente>>(service.listar(), HttpStatus.OK);
	}
	
	@ApiOperation("Retorna una lista paginada de pacientes.")
	@GetMapping(value="/paginado",produces= MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<Page<Paciente>> listarPageable (Pageable pageable){
		return new ResponseEntity<Page<Paciente>>(service.listarPageable(pageable), HttpStatus.OK);
	}	
	
	@ApiOperation("Retorna un paciente por su ID.")
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Paciente> listarId(@PathVariable("id") Integer id){
		Paciente obj = service.listarId(id);
		if (obj == null){
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<Paciente>(service.listarId(id), HttpStatus.OK);
	}
	
	/*
	 * @Valid PERMITE VALIDAR EL JSON ENTRANTE
	 * */
	
	@ApiOperation("Permite el registro de un paciente.")
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Paciente> registrar(@Valid @RequestBody Paciente obj){
		return new ResponseEntity<Paciente>(service.registrar(obj), HttpStatus.CREATED);
	}
	
	@ApiOperation("Permite la modificacion de un paciente.")
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Paciente> modificar (@Valid @RequestBody Paciente obj){
		return new ResponseEntity<Paciente>(service.modificar(obj), HttpStatus.OK);
	}
	
	@ApiOperation("Permite la eliminación de un paciente.")
	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable("id") Integer id){
		Paciente obj = service.listarId(id);
		if (obj == null){
			throw new ModeloNotFoundException("ID: " + id);
		}
		else{
			service.eliminar(id);
		}
	}
}
