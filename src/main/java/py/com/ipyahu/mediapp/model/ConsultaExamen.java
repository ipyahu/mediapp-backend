package py.com.ipyahu.mediapp.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import py.com.ipyahu.mediapp.model.pk.ConsultaExamenPK;

@Entity
@Table(name="consultas_examen")
@IdClass(ConsultaExamenPK.class) //Aqui anotamos esta clase "ENTIDAD" que va a usar la clase PK embebida "ConsultaExamenPK"
public class ConsultaExamen {
	
	@Id
	private Examen examen;
	
	@Id
	private Consulta consulta;

	public Examen getExamen() {
		return examen;
	}

	public void setExamen(Examen examen) {
		this.examen = examen;
	}

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}
}
