package py.com.ipyahu.mediapp.model;

import java.sql.Blob;

public class Menu {

	private Integer idMenu;
	private String nombre;
	private String url;
	private Blob icono;
	
	public Integer getIdMenu() {
		return idMenu;
	}
	public void setIdMenu(Integer idMenu) {
		this.idMenu = idMenu;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Blob getIcono() {
		return icono;
	}
	public void setIcono(Blob icono) {
		this.icono = icono;
	}
}
