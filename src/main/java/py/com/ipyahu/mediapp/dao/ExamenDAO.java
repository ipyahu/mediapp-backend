package py.com.ipyahu.mediapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import py.com.ipyahu.mediapp.model.Examen;

public interface ExamenDAO extends JpaRepository<Examen, Integer>{

}
