package py.com.ipyahu.mediapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import py.com.ipyahu.mediapp.model.Consulta;

@Repository //Estereotipar la interface con Repository es opcional, dado al extender la interface de JpaRepository; Spring ya sabe que esta interface puede ser inyectada
public interface ConsultaDAO extends JpaRepository<Consulta, Integer>{

}
