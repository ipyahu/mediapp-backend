package py.com.ipyahu.mediapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.ipyahu.mediapp.dao.EspecialidadDAO;
import py.com.ipyahu.mediapp.model.Especialidad;
import py.com.ipyahu.mediapp.service.EspecialidadService;

@Service
public class EspecialidadServiceImpl implements EspecialidadService{

	@Autowired
	private EspecialidadDAO dao;
	
	@Override
	public Especialidad registrar(Especialidad obj) {
		return dao.save(obj);
	}

	@Override
	public Especialidad modificar(Especialidad obj) {
		return dao.save(obj);
	}

	@Override
	public List<Especialidad> listar() {
		return dao.findAll();
	}

	@Override
	public Especialidad listarId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public void eliminar(Integer id) {
		dao.delete(id);
	}

}
