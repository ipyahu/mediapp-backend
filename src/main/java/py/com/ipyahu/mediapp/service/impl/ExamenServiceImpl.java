package py.com.ipyahu.mediapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.ipyahu.mediapp.dao.ExamenDAO;
import py.com.ipyahu.mediapp.model.Examen;
import py.com.ipyahu.mediapp.service.ExamenService;

@Service
public class ExamenServiceImpl implements ExamenService{

	@Autowired
	private ExamenDAO dao;
	
	@Override
	public Examen registrar(Examen obj) {
		return dao.save(obj);
	}

	@Override
	public Examen modificar(Examen obj) {
		return dao.save(obj);
	}

	@Override
	public List<Examen> listar() {
		return dao.findAll();
	}

	@Override
	public Examen listarId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public void eliminar(Integer id) {
		dao.delete(id);
	}

}
