package py.com.ipyahu.mediapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import py.com.ipyahu.mediapp.dao.PacienteDAO;
import py.com.ipyahu.mediapp.model.Paciente;
import py.com.ipyahu.mediapp.service.PacienteService;

@Service
public class PacienteServiceImpl implements PacienteService{

	@Autowired
	private PacienteDAO dao;
	
	@Override
	public Paciente registrar(Paciente obj) {
		return dao.save(obj);
	}

	@Override
	public Paciente modificar(Paciente obj) {
		return dao.save(obj);
	}

	@Override
	public List<Paciente> listar() {
		return dao.findAll();
	}

	@Override
	public Paciente listarId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public void eliminar(Integer id) {
		dao.delete(id);
	}

	//metodo para listar de manera paginada
	@Override
	public Page<Paciente> listarPageable(Pageable pageable) {
		return dao.findAll(pageable);
	}
	
	

}
