package py.com.ipyahu.mediapp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import py.com.ipyahu.mediapp.model.Paciente;

public interface PacienteService extends CRUD<Paciente>{
	//En esta interface podemos definir metodos que no sean propios de la interface CRUD
	
	
	//Este metodo nos permite retornar los datos de manera paginada
	Page<Paciente> listarPageable(Pageable pageable);
}
